<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<?php print $head; ?>
	<title><?php print $head_title ?></title>

	<?php print $styles; ?>
	<?php print $scripts; ?> 

<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet"> 

</head>

<body class="<?php print $classes; ?> " <?php print $attributes; ?>>
	<?php print $page_top; ?>

	<?php print $page; ?>

	<?php print $page_bottom; ?>

</body>
</html>
